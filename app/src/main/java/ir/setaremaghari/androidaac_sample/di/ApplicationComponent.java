/*
 * *
 *  * Copyright (C) 2017 Ryan Kay Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package ir.setaremaghari.androidaac_sample.di;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import ir.setaremaghari.androidaac_sample.view.create.NoteCreateFragment;
import ir.setaremaghari.androidaac_sample.view.detail.NoteDetailFragment;
import ir.setaremaghari.androidaac_sample.view.list.NoteListFragment;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Annotated as a Singelton since we don't want to have multiple instances of a Single Database,
 * <p>
 * Created by R_KAY on 8/15/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class, RoomModule.class})
public interface ApplicationComponent {

    void inject(NoteListFragment listFragment);
    void inject(NoteCreateFragment createFragment);
    void inject(NoteDetailFragment detailFragment);

    Application application();

}
