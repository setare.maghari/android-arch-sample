/*
 * *
 *  * Copyright (C) 2017 Ryan Kay Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package ir.setaremaghari.androidaac_sample.view.detail;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ir.setaremaghari.androidaac_sample.R;
import ir.setaremaghari.androidaac_sample.model.Note;
import ir.setaremaghari.androidaac_sample.util.ArchSampleApp;
import ir.setaremaghari.androidaac_sample.viewmodel.NoteDetailViewModel;
import javax.inject.Inject;



public class NoteDetailFragment extends LifecycleFragment {

    private static final String EXTRA_ITEM_ID = "EXTRA_ITEM_ID";

    private TextView dateAndTime;
    private TextView message;
    private ImageView coloredBackground;

    private String itemId;



    @Inject
    ViewModelProvider.Factory viewModelFactory;

    NoteDetailViewModel listItemViewModel;


    public NoteDetailFragment() {
    }


    public static NoteDetailFragment newInstance(String itemId) {
        NoteDetailFragment fragment = new NoteDetailFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_ITEM_ID, itemId);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((ArchSampleApp) getActivity().getApplication())
                .getApplicationComponent()
                .inject(this);

        Bundle args = getArguments();

        this.itemId = args.getString(EXTRA_ITEM_ID);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Set up and subscribe (observe) to the ViewModel

        /*********************************************/
        listItemViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(NoteDetailViewModel.class);

        listItemViewModel.getNoteByID(itemId).observe(this, new Observer<Note>() {
            @Override
            public void onChanged(@Nullable Note note) {
                dateAndTime.setText(note.getNoteId());
                message.setText(note.getMessage());
                coloredBackground.setImageResource(note.getColorResource());
            }
        });
        /*********************************************/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail, container, false);

        dateAndTime = (TextView) v.findViewById(R.id.lbl_date_and_time_header);

        message = (TextView) v.findViewById(R.id.lbl_message_body);


        coloredBackground = (ImageView) v.findViewById(R.id.imv_colored_background);

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}



















