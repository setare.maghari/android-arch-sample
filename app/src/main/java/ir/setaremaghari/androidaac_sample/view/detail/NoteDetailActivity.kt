package ir.setaremaghari.androidaac_sample.view.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ir.setaremaghari.androidaac_sample.R
import ir.setaremaghari.androidaac_sample.R.id
import ir.setaremaghari.androidaac_sample.util.BaseActivity
import ir.setaremaghari.androidaac_sample.util.BaseActivity.Companion
import ir.setaremaghari.androidaac_sample.view.list.NoteListFragment

class NoteDetailActivity : AppCompatActivity() {

  companion object {
    val TAG_LIST_FRAG = "DETAIL_FRAG"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    val manager = supportFragmentManager
    var fragment = manager.findFragmentByTag(TAG_LIST_FRAG)
    if (fragment == null) fragment = NoteListFragment.newInstance()
    BaseActivity.addFragmentToActivity(manager, fragment, id.root_activity_detail, TAG_LIST_FRAG)

  }
}
