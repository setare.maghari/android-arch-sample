package ir.setaremaghari.androidaac_sample.view.create

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ir.setaremaghari.androidaac_sample.R
import ir.setaremaghari.androidaac_sample.util.BaseActivity
import ir.setaremaghari.androidaac_sample.view.list.NoteListFragment

class NoteCreateActivity : BaseActivity() {
  companion object {
    val TAG_CREATE = "CREATE_FRAG"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_create)

    val manager = supportFragmentManager
    var fragment = manager.findFragmentByTag(TAG_CREATE)
    if (fragment == null) fragment = NoteCreateFragment.newInstance()
    addFragmentToActivity(manager, fragment, R.id.root_activity_create, TAG_CREATE)

  }
}
