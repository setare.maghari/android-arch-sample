package ir.setaremaghari.androidaac_sample.view.list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ir.setaremaghari.androidaac_sample.R
import ir.setaremaghari.androidaac_sample.util.BaseActivity

class NoteListActivity : BaseActivity() {

  companion object {
    val TAG_LIST_FRAG = "LIST_FRAG"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_list)
    val manager = supportFragmentManager
    var fragment = manager.findFragmentByTag(TAG_LIST_FRAG)
    if (fragment == null) fragment = NoteListFragment.newInstance()
    addFragmentToActivity(manager, fragment, R.id.root_activity_list, TAG_LIST_FRAG)

  }
}
