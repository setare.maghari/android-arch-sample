package ir.setaremaghari.androidaac_sample.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity

/**
 * Created by Setare on 11/9/2017.
 */
open class BaseActivity : AppCompatActivity() {
  companion object {
    fun addFragmentToActivity(fragmentManager: FragmentManager,
        fragment: Fragment, fragmentId: Int, tag: String
        )
    {
      var transaction = fragmentManager.beginTransaction()
      transaction.replace(fragmentId, fragment, tag)
      transaction.commit()
    }
  }
}