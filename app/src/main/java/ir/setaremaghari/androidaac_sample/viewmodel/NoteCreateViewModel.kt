package ir.setaremaghari.androidaac_sample.viewmodel

import android.arch.lifecycle.ViewModel
import ir.setaremaghari.androidaac_sample.model.Note
import ir.setaremaghari.androidaac_sample.model.NoteRepository
import org.jetbrains.anko.doAsync

/**
 * Created by Setare on 11/10/2017.
 */

/*
* be ezaye har View ma ye View Model darim ke tain mikonim
* in view be che chizai az model ma dastresi dare
* masalan inja faghat gharare ke note besazim
* pas az noteRepo faghat darj ro seda mikonim
* */

/*
* be viewModel mishe har constructori pass dad
* faghat hargez nabayad Context e ye activity ro dad behesh
* chon agar activity masalan rotate she ViewModel
* be ye chizi vasle ke dige nist, va baese memory lea
* */
class NoteCreateViewModel(var noteRepository: NoteRepository) : ViewModel() {

  fun insertNote(note: Note){
    doAsync {
      noteRepository.insertNote(note)
    }
  }

}