package ir.setaremaghari.androidaac_sample.viewmodel

import android.arch.lifecycle.ViewModel
import ir.setaremaghari.androidaac_sample.model.NoteRepository

/**
 * Created by Setare on 11/10/2017.
 */
class NoteDetailViewModel(var noteRepository: NoteRepository): ViewModel() {

  fun getNoteByID(noteID: String) = noteRepository.getNoteById(noteID)

}