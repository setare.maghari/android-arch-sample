package ir.setaremaghari.androidaac_sample.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import ir.setaremaghari.androidaac_sample.model.Note
import ir.setaremaghari.androidaac_sample.model.NoteRepository
import org.jetbrains.anko.doAsync

/**
 * Created by Setare on 11/10/2017.
 */
class NoteListViewModel(var noteRepository: NoteRepository): ViewModel() {

  /*TODO chegune getNotes ro async konam
  * */
  fun getNotes(): LiveData<List<Note>> = noteRepository.getNotes()

  fun deleteNote(note: Note){
    doAsync {
      noteRepository.deleteNote(note)
    }
  }

}