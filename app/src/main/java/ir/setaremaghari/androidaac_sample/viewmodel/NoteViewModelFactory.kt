package ir.setaremaghari.androidaac_sample.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import ir.setaremaghari.androidaac_sample.model.NoteRepository

/**
 * Created by Setare on 11/10/2017.
 */


/*
* sakhtane in del bekhahie
* mishe jaye in ba dagger ViewModel marbut be har view
* ro dakhele khodesh inject kard
*
*
* in injuri kar mikone ke behesh class i ke hastimo midim
* in bejash ViewModel i ke un class mikhado behesh barmigardune
* */

class NoteViewModelFactory(var noteRepository: NoteRepository) : ViewModelProvider.Factory {



  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    return when {
      modelClass.isAssignableFrom(NoteCreateViewModel::class.java) -> NoteCreateViewModel(noteRepository) as T
      modelClass.isAssignableFrom(NoteDetailViewModel::class.java) -> NoteDetailViewModel(noteRepository) as T
      modelClass.isAssignableFrom(NoteListViewModel::class.java) -> NoteListViewModel(noteRepository) as T
      else -> throw IllegalArgumentException("ViewModel Not Found")
    }
  }




}