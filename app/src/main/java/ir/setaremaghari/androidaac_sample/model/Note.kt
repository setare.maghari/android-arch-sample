package ir.setaremaghari.androidaac_sample.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Setare on 11/8/2017.
 */


/*
* Class Haye Entity ba @Entity moshakhas mikonim
* inja mishe moshakhas konim primarykey mun ham chi hast
* */
@Entity
data class Note(@PrimaryKey var noteId: String ="",
    var message: String = "",
    var colorResource:Int = 1): Serializable