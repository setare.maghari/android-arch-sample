package ir.setaremaghari.androidaac_sample.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Created by Setare on 11/8/2017.
 */

/*
* hame karaye marbut be DB ba ers bari az RoomDatabase
* anjam mishan
*
* va entity ro barash moshakhas mikonim ke kudume
* */
@Database(entities = arrayOf(Note::class), version = 1)
abstract class NoteDb : RoomDatabase(){
  abstract fun noteDao() : NoteDao
}
