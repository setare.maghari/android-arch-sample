/*
 * *
 *  * Copyright (C) 2017 Ryan Kay Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package ir.setaremaghari.androidaac_sample.model

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query


/*
* baraye har entity ye Dao misazim(Data Access Object)
* ke kheyli sade miaym query hamun ro baraye un entity moshakhas mikonim
* */
@Dao
interface NoteDao {

  @Query("SELECT * FROM Note")
  fun getNotes() : LiveData<List<Note>>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertNote(note: Note): Long

  @Delete
  fun deleteNote(note: Note) : Unit

  @Query("SELECT * FROM Note WHERE noteId = :noteId")
  fun getNoteById(noteId: String) : LiveData<Note>
}
