package ir.setaremaghari.androidaac_sample.model

import android.arch.lifecycle.LiveData
import javax.inject.Inject

/**
 * Created by Setare on 11/9/2017.
 */

/*
* Repository faghat tain mikone ke data chejuri daryaft dare mishe
* az Webservice, Db ya..
* nokte ine ke az repository bayad LiveData<Entity> bargarde
* ke ba har beruz resani samte ui ham update she
* */
class NoteRepository @Inject constructor(val noteDao: NoteDao){

  fun getNotes() : LiveData<List<Note>>{
    
    return noteDao.getNotes()
  }

  fun insertNote(note: Note): Long{
    return noteDao.insertNote(note)
  }

  fun deleteNote(note: Note) : Unit{
    noteDao.deleteNote(note)
  }

  fun getNoteById(itemId: String) : LiveData<Note>{
    return noteDao.getNoteById(itemId)
  }
}